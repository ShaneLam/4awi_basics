package at.car;

public class Producer {
	private String Name;
	private String Country;
	private double discount;
	
	public Producer(String name, String country, double discount) {
		super();
		Name = name;
		Country = country;
		this.discount = discount;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getCountry() {
		return Country;
	}

	public void setCountry(String country) {
		Country = country;
	}

	public double getDiscount() {
		return discount;
	}

	public void setDiscount(double discount) {
		this.discount = discount;
	}
	
	

}
