package at.car;

public class Engine {
	private String FuelType;
	private float kw;
	
	public Engine(String FuelType, float kw) {
		super();
		this.FuelType = FuelType;
		this.kw = kw;
	}


	public String getFuelType() {
		return FuelType;
	}


	public void setFuelType(String fuelType) {
		FuelType = fuelType;
	}


	public float getKw() {
		return kw;
	}

	public void setKw(float kw) {
		this.kw = kw;
	}
	
	
}
