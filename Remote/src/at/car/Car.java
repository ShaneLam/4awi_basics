package at.car;

public class Car {
	private String Colour;
	private float Price;
	private float Topspeed;
	private double Usage;
	private float km;
	private Producer Producer;
	private Engine Engine;
	
	public Engine getEngine() {
		return Engine;
	}

	public void setEngine(Engine engine) {
		Engine = engine;
	}

	public Car(at.car.Engine engine) {
		super();
		Engine = engine;
	}

	public Car(at.car.Producer producer) {
		super();
		Producer = producer;
	}

	public Producer getProducer() {
		return Producer;
	}

	public void setProducer(Producer producer) {
		Producer = producer;
	}


	public Car(String colour, float price, float topspeed, double usage, float km, Producer Producer, Engine Engine) {
		super();
		Colour = colour;
		Price = price;
		Topspeed = topspeed;
		Usage = usage;
		this.km = km;
		this.Producer = Producer;
		this.Engine = Engine;
	}

	public String getColour() {
		return Colour;
	}

	public void setColour(String colour) {
		Colour = colour;
	}

	public float getPrice() {
		return Price;
	}

	public void setPrice(float price) {
		Price = price;
	}

	public float getTopspeed() {
		return Topspeed;
	}

	public void setTopspeed(float topspeed) {
		Topspeed = topspeed;
	}

	public double getUsage() {
		return Usage;
	}

	public void setUsage(double usage) {
		Usage = usage;
	}

	public float getKm() {
		return km;
	}

	public void setKm(float km) {
		this.km = km;
	}
		
	public double Usage() {
		if(this.km < 50000) {
			this.Usage = 7;
		}else {
			this.Usage = 7 * 1.098;
		}
		return Usage;
	}
	
	public double Price() {
		return (Producer.getDiscount()/100) * this.Price;
			
	}
	
	public String getType() {
		return Engine.getFuelType();
	}
	
	


}
