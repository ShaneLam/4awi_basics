package at.car;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

public class Person {
	private String firstName;
	private String lastName;
	private LocalDate today = LocalDate.now();
	private LocalDate birthday;
	private List<Car> cars;

	public String getFirstname() {
		return firstName;
	}

	public void setFirstname(String firstname) {
		firstName = firstname;
	}

	public String getLastname() {
		return lastName;
	}

	public void setLastname(String lastname) {
		lastName = lastname;
	}



	public List<Car> getCars() {
		return cars;
	}

	public void setCars(List<Car> cars) {
		this.cars = cars;
	}

	public Person(String firstname, String lastname, LocalDate birthday) {
		super();
		firstName = firstname;
		lastName = lastname;
		this.birthday = birthday;

		this.cars = new ArrayList<>();
	}

	public void addCars(Car car) {
		this.cars.add(car);
	}

	public double getValue() {
		double Value = 0;
		for (Car Car : cars) {
			Value = Value + Car.Price();
		}
		return Value;

	}

	public int getDate() {
		Period p = Period.between(birthday, today);
		return p.getYears();
	}

}
