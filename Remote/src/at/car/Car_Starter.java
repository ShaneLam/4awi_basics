package at.car;

import java.time.LocalDate;
import java.time.Month;


public class Car_Starter {

	public static void main(String[] args) {
		
		Engine e1 = new Engine("Diesel", 120);
		Producer p1 = new Producer("BMW", "Germany", 50);
		Car c1 = new Car("black", 100, 120, 7, 2000, p1, e1);
		Person P1 = new Person("Shane", "Lampert",LocalDate.of(1930, Month.SEPTEMBER, 11));
		P1.addCars(c1);
		
		
		Engine e2 = new Engine("Benzin", 150);
		Producer p2 = new Producer("Honda", "Austria", 50);
		Car c2 = new Car("red", 100, 120, 7, 5000, p2, e2);
		P1.addCars(c2);
		
		System.out.println(c1.Usage());
		System.out.println(c1.Price());
		System.out.println(c1.getType());
		System.out.println(P1.getValue());
		System.out.println(P1.getDate());
	}

}
